# Психоэмоциональный тренажер #emmo2072
Аппаратно-программный комплекс (АПК) предназначен для восстановления работы мимических паттернов, отвечающих за эмоции, у людей, перенесших инсульт.
АПК состоит из:
1. Однопалатного компьютера Odroid n2.
2. Платы миостимулятора с электродами.
3. Маски, обеспечивающей прилегание электродов миостимулятора к мимическим мышцам лица.
4. Программного обеспечения, состоящего из нейросеть на базе tensorflow и интерфейсной программы на python, обеспечивающей управление платой миостимулятора.

**Требования:**

* Raspberry pi 3 model B+ или odroid n2
* Raspbian 10 Buster 10.08.2019
* Камера USB или CSI
* Последнии версии программ и рабочий репозиторий raspbian

*Примечание: возможна работа на Debian, Ubuntu, Mint и производных.*

**Инструкция для raspberry pi:**

Скачайте [архив](https://gitlab.com/wotblitza/emotionrecognation/-/archive/master/emotionrecognation-master.zip) программы с репозитория

Распакуйте его.

Скачайте [модель для нейросети](https://drive.google.com/file/d/1w8GDMgByatYexp3FCFKntTwHnYiSt-ez/view?usp=sharing) и положите в распакованную папку.

Проверте работу репозитория raspbian командой (если выдается ошибка, значит репозиторий не работает):

*sudo apt update* 

Установите обновления:

*sudo apt upgrade*

Измените размер файла подкачки командой:

*sudo nano /etc/dphys-swapfile*

Внутри него нужно изменить значение переменной *CONF_SWAPSIZE*. Установить *2048* вместо *100* установленных. 

Перезагрузите систему для применения изменений.

**Установка с помощью скрипта:**

*Raspberry pi*

Разрешите выполнения установочного скрипта:

*sudo chmod u+x ./scripts/rasppi/install.sh*

Запустите его (*не от root!*):

*./scripts/rasppi/install.sh*

**Odroid n2**

Разрешите выполнения установочного скрипта:

*sudo chmod u+x ./scripts/odroid/install.sh*

Запустите его (*не от root!*):

*./scripts/odroid/install.sh*

**Установка вручную:**

**Raspberry pi**

Установите OpenCV

*sudo apt install python3-opencv*

*sudo apt-get install libjpeg-dev libpng-dev libtiff-dev -y*

*sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y*

*sudo apt-get install libxvidcore-dev libx264-dev -y*

*sudo apt-get install libatlas-base-dev gfortran -y*


Установите пакет  Python дял разработки

*sudo apt-get install python3-dev -y*

Установите Tkinter

*sudo apt install python3-tk*

Устанвоите Pillow

*pip3 install --upgrade Pillow*

Устанвоите libhdf5

*sudo apt install libhdf5-dev -y*

Установите scipy

*sudo apt install python3-scipy*

Установите RPIO

*sudo apt install python3-rpi.gpio*

Установите tflearn,h5py,tensorflow (*Не от root!*)

*pip3 install tflearn h5py tensorflow*

**Запуск программы:**

*Через терминал:*

*python3 run.py*

*Через скрипт:*

Разрешите выполнение скрипта (*Это делается один раз!*)

*sudo chmod u+x ./run.sh*

В дальнейшем скрипт должен работать от двойново клика мыши.

*Если скрипт не запустился:*

Попробуйте запусустить через терминал скрипт:

*./run.sh*

Попробуйте запустить программу через терминал.