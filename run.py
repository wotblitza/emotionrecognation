import cv2
import sys
from em_model import EMR
import numpy as np
import subprocess
from tkinter import *
from PIL import ImageTk
##Massive of emotions
EMOTIONS = ['angry', 'disgusted', 'fearful', 'happy', 'sad', 'surprised', 'neutral'] 
# Resolution calculation
cmd = ['xrandr']
cmd2 = ['grep', '*']
p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
p.stdout.close()
resolution_string, junk = p2.communicate()
resolution = resolution_string.split()[0]
resolution = resolution.decode("utf-8") 
width = int(resolution.split("x")[0].strip())
heigth = int(width/1.4)
#Botton interaction
def clicked():
    print("Clicked!")



cascade_classifier = cv2.CascadeClassifier('haarcascade_files/haarcascade_frontalface_default.xml')

def brighten(data,b):
     datab = data * b
     return datab    

def format_image(image):
  if len(image.shape) > 2 and image.shape[2] == 3:
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  else:
    image = cv2.imdecode(image, cv2.CV_LOAD_IMAGE_GRAYSCALE)
  faces = cascade_classifier.detectMultiScale(
      image,
      scaleFactor = 1.3 ,
      minNeighbors = 5
  )

##Counting number of faces
  if not len(faces) > 0:
    return None
  max_area_face = faces[0]
  for face in faces:
    if face[2] * face[3] > max_area_face[2] * max_area_face[3]:
      max_area_face = face
  face = max_area_face
  image = image[face[1]:(face[1] + face[2]), face[0]:(face[0] + face[3])]
  try:
    image = cv2.resize(image, (48,48), interpolation = cv2.INTER_CUBIC) / 255.
  except Exception:
    print("[+] Problem during resize")
    return None
  return image
#Radiobuttons
def change():
    if var.get() == 0:
        label['bg'] = 'low'
    elif var.get() == 1:
        label['bg'] = 'medium'
    elif var.get() == 2:
        label['bg'] = 'high'


network = EMR()
network.build_network()
#Camera capture
video_capture = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_SIMPLEX
#Emtions loading
feelings_faces = []
for index, emotion in enumerate(EMOTIONS):
  feelings_faces.append(cv2.imread('./emojis/' + emotion + '.png', -1))


#Panel creation
width2 = int(heigth*0.3)
resolution = (str(width2) + 'x' + str(heigth))
root = Tk()
angry = ImageTk.PhotoImage(file="./emojis/angry.png")
happy = ImageTk.PhotoImage(file="./emojis/happy.png")
disgusted = ImageTk.PhotoImage(file="./emojis/disgusted.png")
fearful = ImageTk.PhotoImage(file="./emojis/fearful.png")
neutral = ImageTk.PhotoImage(file="./emojis/neutral.png")
sad = ImageTk.PhotoImage(file="./emojis/sad.png")
reset = ImageTk.PhotoImage(file="./emojis/reset.png")
surprised = ImageTk.PhotoImage(file="./emojis/surprised.png")
root.title("Control Panel")
root.geometry(resolution)
btnhappy = Button(root, image=happy, command=clicked) 
btnhappy.grid(column=0, row=1)
lblhappy = Label(root, text="Happy")  
lblhappy.grid(column=0, row=2)  
btnangry = Button(root, image=angry, command=clicked) 
btnangry.grid(column=1, row=1)
lblangry = Label(root, text="Angry")  
lblangry.grid(column=1, row=2)
btndisgusted = Button(root, image=disgusted, command=clicked) 
btndisgusted.grid(column=0, row=3)
lbldisgusted = Label(root, text="Disgusted")  
lbldisgusted.grid(column=0, row=4)
btnfearful = Button(root, image=fearful, command=clicked) 
btnfearful.grid(column=1, row=3)
lblfearful = Label(root, text="Fearful")  
lblfearful.grid(column=1, row=4)
btnneutral = Button(root, image=neutral, command=clicked) 
btnneutral.grid(column=0, row=5)
lblneutral = Label(root, text="Neutral")  
lblneutral.grid(column=0, row=6)
btnsad = Button(root, image=sad, command=clicked) 
btnsad.grid(column=1, row=5)
lblsad = Label(root, text="Sad")  
lblsad.grid(column=1, row=6)
btnsurprised = Button(root, image=surprised, command=clicked) 
btnsurprised.grid(column=0, row=7)
lblsurprised = Label(root, text="Surprised")  
lblsurprised.grid(column=0, row=8)
btnreset = Button(root, image=reset, command=clicked) 
btnreset.grid(column=1, row=7)
lblreset = Label(root, text="Reset")  
lblreset.grid(column=1, row=8)
iswork = IntVar()
iswork_checkbutton = Checkbutton(text="Enable Mio", font=("Liberation Sans", 12), variable=iswork)
iswork_checkbutton.grid(column=0, row=10)
lblnapr = Label(root, text="Amperage", font=("Liberation Sans", 12))  
lblnapr.grid(column=0, row=11)
#Variables of energy level (Amperage)
var = IntVar()
var.set(0)
low = Radiobutton(text="Low", variable=var, value=0)
medium = Radiobutton(text="Medium", variable=var, value=1)
high = Radiobutton(text="High", variable=var, value=2)
low.grid(column=0, row=12)
medium.grid(column=0, row=13)
high.grid(column=0, row=14)

#GUI
while True:
  #root.lift()
  root.update_idletasks()
  root.update()
  ret, frame = video_capture.read()
  result = network.predict(format_image(frame))
  
  if result is not None:
    for index, emotion in enumerate(EMOTIONS):
      cv2.putText(frame, emotion, (10, index * 20 + 20), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1);
      cv2.rectangle(frame, (130, index * 20 + 10), (130 + int(result[0][index] * 100), (index + 1) * 20 + 4), (255, 0, 0), -1)

    face_image = feelings_faces[result[0].tolist().index(max(result[0]))]
    for c in range(0, 3):
      frame[200:320, 10:130, c] = face_image[:,:,c] * (face_image[:, :, 3] / 255.0) +  frame[200:320, 10:130, c] * (1.0 - face_image[:, :, 3] / 255.0)
  window = cv2.resize(frame, (width, heigth)) 
  cv2.imshow('ICCET-2019 #emmo2072', frame)
  
#Exit on q button
  if cv2.waitKey(1) & 0xFF == ord('q'):
    break
#Programm turn off
video_capture.release()

cv2.destroyAllWindows()
