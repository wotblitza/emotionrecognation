cd ~
git clone https://gitlab.com/wotblitza/emotionrecognation
export fileid=1w8GDMgByatYexp3FCFKntTwHnYiSt-ez
export filename=model.tflearn.data-00000-of-00001
wget --save-cookies cookies.txt 'https://docs.google.com/uc?export=download&id='$fileid -O- \
     | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1/p' > confirm.txt

wget --load-cookies cookies.txt -O $filename \
     'https://docs.google.com/uc?export=download&id='$fileid'&confirm='$(<confirm.txt)
mv model.tflearn.data-00000-of-00001 emotionrecognation/
sudo apt update -y
sudo apt upgrade -y
sudo apt install python3-opencv -y
sudo apt-get install libjpeg-dev libpng-dev libtiff-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install python3-dev -y
sudo apt install python3-tk -y
sudo apt install libhdf5-dev -y
sudo apt install python3-scipy -y
sudo apt install python3-h5py -y
sudo apt install python3-pip -y
pip3 install setuptools wheel
pip3 install Pillow
pip3 install tflearn
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.14.0-buster/tensorflow-1.14.0-cp37-none-linux_aarch64.whl
pip3 install tensorflow-1.14.0-cp37-none-linux_aarch64.whl
sudo nano ~/.local/lib/python3.7/site-packages/tensorflow/contrib/__init__.py
