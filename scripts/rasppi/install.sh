sudo apt-get update && sudo apt-get upgrade
sudo apt-get install build-essential cmake unzip pkg-config -y
sudo apt-get install libjpeg-dev libpng-dev libtiff-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libgtk-3-dev -y
sudo apt-get install libcanberra-gtk* -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install python3-dev -y
sudo apt install python3-opencv -y
sudo apt install python3-scipy -y
sudo apt install libhdf5-dev -y
sudo apt install python3-tk
pip3 install --upgrade Pillow 
pip3 install h5py tflearn tensorflow
python3 -m pip install tensorflow